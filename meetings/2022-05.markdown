meeting_title: "Linux on RISC-V"
meeting_datetime: 2022-05-10T19:30:00
meeting_location: "Virtual - Big Blue Button"
meeting_speaker: "Drew Fustini"
meeting_schedule_template: _schedules/virtual.html

It is an exciting time for Linux on RISC-V, the open instruction set (ISA) that is quickly gaining critical mass. I will introduce the pieces needed to boot Linux on RISC-V including the Privileged Architecture, OpenSBI and U-Boot, and how that fits into the upcoming RISC-V Platform Specification. I will break down support for existing hardware and current upstreaming efforts.

We're going to use Big Blue Button for this meeting:

**Time:**  May 10, 2022 07:30 PM Eastern Time

**Join Big Blue Button:** https://blue.lpi.org/b/eva-zjc-gjy-kgl
