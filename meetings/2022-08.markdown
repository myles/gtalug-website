meeting_title: "Fire Side Chat!"
meeting_datetime: 2022-08-09T19:30:00
meeting_location: "Virtual - Big Blue Button"
meeting_speaker: "You"
meeting_schedule_template: _schedules/virtual.html

Bring your questions, your answers, and your stories to sare with your fellow GTALUG members.

We're going to use Big Blue Button for this meeting:

**Time:**  August 09, 2022 07:30 PM Eastern Time

**Join Big Blue Button:** https://blue.lpi.org/b/eva-zjc-gjy-kgl

