meeting_title: "Annual General Meeting"
meeting_datetime: 2022-12-13T19:30:00
meeting_location: "Virtual - Big Blue Button"
meeting_speaker: ""
meeting_schedule_template: _schedules/virtual.html

AGM followed by a discussion about GTALUG technology now and in the future.

We're going to use Big Blue Button for this meeting:

**Time:**  December 13, 2022 07:30 PM Eastern Time

**Join Big Blue Button:** https://blue.lpi.org/b/eva-zjc-gjy-kgl
