meeting_title: "Lightning talks and Round Table"
meeting_datetime: 2020-07-14T19:30:00
meeting_location: "Jitsi meeting"
meeting_schedule_template: _schedules/virtual.html



This is GTALUG's version of an un-conference, a loosely structured short talks emphasizing the informal exchange of information and ideas between participants, rather than following a conventionally structured GTALUG meetings.

If you already have a topic in mind please send an email to  [speakers@gtalug.org](mailto:speakers@gtalug.org) to be added to the list of scheduled talks.

A Round Table Q&A Session is where we take questions from the audience, and the audience discusses and attempts to answer those questions from their own knowledge. It's a great way to meet fellow members of our community and discover the skill sets we each bring to the table.

Due to global pandemic we are going to hold this session via video conferencing.

We're going to use jitsi for this meeting:

**Meeting room url:** [https://jitsi.flamy.ca/GTALUG](https://jitsi.flamy.ca/GTALUG)

** Lightly edited meeting material: [GTALUG 2020-07 Meeting Notes](https://linuxdatabases.info/blog/wp-admin/post.php?post=247)

** Notes on [Etherpad - GTALUG - July](https://etherpad.wikimedia.org/p/gtalug-july)
