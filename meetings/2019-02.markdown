meeting_title: "FPGAs for Fun, Acceleration, and Profit!"
meeting_datetime: 2019-02-12T19:30:00
meeting_speaker: "Chris Tyler"
meeting_location: "George Vari Engineering and Computing Centre, Room 203"
meeting_location_template: _locations/r-gvecc-203.html
meeting_schedule_template: _schedules/2015-10.html

# CANCELLED - Due to adverse weather.

<strike>

Field Programmable Gate Arrays (FPGAs) have moved from being expensive, exotic
prototyping tools to affordable mainstream devices for hardware customization
and acceleration. This talk takes a general look at FPGAs, how to use them, the
current technology landscape, the increasing use of FPGAs as cloud, leaf, and
server accelerators, and related technology initiatives around standardization
and cache coherence.

</strike>
